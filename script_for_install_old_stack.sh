#! /bin/bash

## start with root
BLACK='\033[30m'		#	${BLACK}		
RED='\033[31m'			#	${RED}			
GREEN='\033[32m'		#	${GREEN}		
YELLOW='\033[33m'		#	${YELLOW}		
BLUE='\033[34m'			#	${BLUE}			
MAGENTA='\033[35m'		#	${MAGENTA}		
CYAN='\033[36m'			#	${CYAN}			
GRAY='\033[37m'	
NORMAL='\033[0m'		#	${NORMAL}

# install requirements
echo "${BLUE} install requirements for moodle installer${NORMAL}"
apt update -y
apt upgrade -y
apt install git -y
apt install curl -y
apt install python-software-properties -y
add-apt-repository ppa:ondrej/php
echo "${GREEN} requirements installed${NORMAL}"

# configure php
apt install php7.2 -y
echo "${GREEN} php7.2 installed${NORMAL}"

# install lamp php 7.2 mysql 5.7.38

apt install tasksel -y
tasksel install lamp-server
echo "${GREEN} tasksel installed${NORMAL}"

# add ufw
ufw app list
ufw app info "Apache Full"
ufw status
ufw allow in "Apache Full"
echo "${GREEN} ufw allowed${NORMAL}"

# add password for root mysql

rm create_sql.sql
echo "${YELLOW}Enter password for root mysql${NORMAL}"
read sql_password
echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$sql_password';" >> create_sql.sql
echo "FLUSH PRIVILEGES;" >> create_sql.sql
mysql -uroot < create_sql.sql
echo "${GREEN} add root password mysql${NORMAL}"

# install phpmyadmin

apt -y install php7.2-mbstring
apt -y install phpmyadmin
echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf 
/etc/init.d/apache2 restart
echo "${GREEN} phpmyadmin installed${NORMAL}"

# install moodle

apt-get update -y
apt-get install aspell graphviz php7.2-curl php7.2-gd php7.2-intl php7.2-ldap php7.2-mysql php7.2-pspell php7.2-xml php7.2-xmlrpc php7.2-zip -y
systemctl restart apache2
git clone https://github.com/moodle/moodle -b MOODLE_35_STABLE /var/www/html/moodle
mkdir /var/moodledata
chown -R www-data /var/moodledata
chmod -R 0770 /var/moodledata
echo "${GREEN} moodle installed${NORMAL}"

# config mysql

mv mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
systemctl restart mysql
echo "${GREEN} mysql configured ${NORMAL}"

# create config for create moodle database

rm create_db.sql
echo "${YELLOW}Enter username for moodledb${NORMAL}"
read moodledb_username
echo "${YELLOW}Enter password for moodledb${NORMAL}"
read moodledb_password
echo "CREATE DATABASE moodle;" >> create_db.sql
echo "CREATE USER '"$moodledb_username"'@'localhost' IDENTIFIED BY '"$moodledb_password"';" >> create_db.sql
echo "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '"$moodledb_username"'@'localhost';" >> create_db.sql
echo "${GREEN} moodle database config created ${NORMAL}"

# create database
mysql -uroot -p"$sql_password" < create_db.sql   # root no passwd
echo "${GREEN} moodle database config created${NORMAL}"




