#! /bin/bash

## start with root

# install requirements
apt update -y
apt install git -y
apt install curl -y

# install nginx
apt install nginx -y

# add nginx to startup
systemctl enable nginx.service

# install Mariadb
apt-get install mariadb-server mariadb-client -y
systemctl enable mariadb.service

# secure settings for Mariadb
echo "Enter password for root user Mariadb"
read root_password_db
echo "Start secure settings"
echo "$root_password_db"
mysql_secure_installation <<EOF
y
"$root_password_db"
"$root_password_db"
y
y
y
y
EOF
echo "Finish secure settings"

# create config for create moodle database
rm create_db.sql
echo "Enter username for moodledb"
read moodledb_username
echo "Enter password for moodledb"
read moodledb_password

echo "CREATE DATABASE moodle;" >> create_db.sql
echo "CREATE USER '"$moodledb_username"'@'localhost' IDENTIFIED BY '"$moodledb_password"';" >> create_db.sql
echo "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '"$moodledb_username"'@'localhost';" >> create_db.sql

# create database
mysql -uroot -p"$root_password_db" < create_db.sql
echo "Database created"

# install php
apt-get install php-fpm php-mysql php-xml

# download moodle
git clone git://git.moodle.org/moodle.git /var/www/html/moodle

# add access and requirements for moodle
echo "Add access and requirements for moodle"
chown www-data:www-data -R /var/www/html/moodle
chmod 775 -R /var/www/html/moodle
mkdir -p /var/moodledata
chmod 775 -R /var/moodledata
chown www-data:www-data -R /var/moodledata

# create hard link
#echo "Create hard link"
#ln -s /etc/nginx/sites-available/moodle /etc/nginx/sites-enabled/

######
cp /var/www/html/moodle/config-dist.php /var/www/html/moodle/config.php 


# restart nginx
echo "Restart nginx"
systemctl restart nginx.service

