#! /bin/bash

apt-get update

apt-get install aspell graphviz php7.2-curl php7.2-gd php7.2-intl php7.2-ldap php7.2-mysql php7.2-pspell php7.2-xml php7.2-xmlrpc php7.2-zip
systemctl restart apache2

# install moodle

git clone https://github.com/moodle/moodle -b MOODLE_35_STABLE /var/www/html/moodle
mkdir /var/moodledata
chown -R www-data /var/moodledata
chmod -R 0770 /var/moodledata

# config mysql

mv mysql.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
systemctl restart mysql

# create config for create moodle database
rm create_db.sql

echo "Enter username for moodledb"
read moodledb_username
echo "Enter password for moodledb"
read moodledb_password

echo "CREATE DATABASE moodle;" >> create_db.sql
echo "CREATE USER '"$moodledb_username"'@'localhost' IDENTIFIED BY '"$moodledb_password"';" >> create_db.sql
echo "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO '"$moodledb_username"'@'localhost';" >> create_db.sql

# create database
mysql -uroot -p"$root_password_db" < create_db.sql   # root no passwd
echo "Database created"
