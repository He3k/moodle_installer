#! /bin/bash

echo "Enter password for root mysql"
read sql_password
echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$sql_password';" >> create_sql.sql
echo "FLUSH PRIVILEGES;" >> create_sql.sql
mysql -uroot < create_sql.sql
echo "${GREEN} add root password mysql${NORMAL}"